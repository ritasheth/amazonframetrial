package com.utilSetup;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class WebMethodNameUtil {
	
	
    private WebDriver driver;
	public WebMethodNameUtil(WebDriver driver) {
		this.driver=driver;
	}	
	
	public void click(By  prop) {
		new WebDriverWait(driver,200).until(ExpectedConditions.visibilityOfElementLocated(prop));
		driver.findElement(prop).click();
	}
	
	public void enterText(By  prop,String testdata) {
		new WebDriverWait(driver,200).until(ExpectedConditions.visibilityOfElementLocated(prop));
		driver.findElement( prop).sendKeys( testdata);
	}
	
	public void enterText(By  prop,CharSequence testdata) {
		new WebDriverWait(driver,200).until(ExpectedConditions.visibilityOfElementLocated(prop));
		driver.findElement( prop).sendKeys( testdata);
	}
	//String expectedData="";
	public String gettext(By prop) {
		new WebDriverWait(driver,200).until(ExpectedConditions.visibilityOfElementLocated(prop));
		WebElement data=driver.findElement(prop);
	    System.out.println(data.getText());
	    return toString();
	   // Assert.assertEquals(actualboolean, expectedboolean);
	}
	
	public void selectDropedownValue(By  prop,String testdata ) {
		new WebDriverWait(driver,200).until(ExpectedConditions.visibilityOfElementLocated(prop));
		WebElement data=driver.findElement(prop);
		Select s=new Select(data);
		s.selectByValue("data");
	}	
		
	public void actionmove(By  prop) {
		new WebDriverWait(driver,200).until(ExpectedConditions.visibilityOfElementLocated(prop));
		WebElement move=driver.findElement(prop);
		Actions s=new Actions(driver);
		s.moveToElement(move).build().perform();
		
	}
	
	  public void close() {
		  driver.quit();

	}	
	}
	
	
	

	
	
	


