package com.actionPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pagesproperties.SigninObjProperties;
import com.utilSetup.WebMethodNameUtil;

public class SigninPage extends WebMethodNameUtil implements SigninObjProperties{   
	
	
	
    
	WebDriver driver;
    
	public SigninPage (WebDriver driver) {
    super(driver);
    this.driver=driver;
	}
	
	
	public void Sign() {
	click(signin);	
	}	
	public void helloSign() {
		click(HelloSign);	
		}
	public void enterEmail() {
		enterText(EmailAddress,"sS"+"@mailinator.com");
		}
	public void enterPassword() {
		enterText(Password,"12345");	
		}
	public void signSubmit() {
		click(ClickSign);	
		}
	public void rememberMe() {
		click(RememberMe);
	}
	
     public String getEnterCharacter() {
    return (gettext(EnterCharacter));
}		
	
	

 //  public void typeCharacter() {
	//EnterText(TypeCharacter,"");	
	//}
     public void Stop() {
     close();
}
}