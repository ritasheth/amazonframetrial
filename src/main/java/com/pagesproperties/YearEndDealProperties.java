package com.pagesproperties;

import org.openqa.selenium.By;

public interface YearEndDealProperties {
	
	
	By YearEnddeal=By.xpath("//*[text()='Year-End Deals'][1]");
    By Fashion=By.xpath("//*[@tabindex='68']");
    By ShopAllDeal=By.xpath("//*[@class='a-section tileOverlay selected']");
    By Remote=By.id("104_dealView_13");
}
