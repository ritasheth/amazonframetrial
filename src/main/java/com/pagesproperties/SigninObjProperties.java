package com.pagesproperties;

import org.openqa.selenium.By;

public interface SigninObjProperties {
	
	
	
	By signin=By.id("nav-hamburger-menu");
    By HelloSign=By.id("hmenu-customer-name");
    By EmailAddress=By.id("ap_email");
    By Password=By.id("ap_password");
    By ClickSign=By.id("signInSubmit");
    By RememberMe=By.xpath("//*[@name='rememberMe']");
    By EnterCharacter=By.id("auth-captcha-image");
    By TypeCharatcer=By.id("auth-captcha-guess");
}
